import React from 'react';

const Comment = (props) => {
    return (
       <div className="comment">
          <a href="/" className="avatar">
            <img alt="avatar" src={props.avatar}/>
          </a>
          <div>
            <a href="/" className="author">{props.author}</a>
            <div className="metadata">
            <span className="date">{props.date}</span>
            </div>
            <div className="text">
	      {props.content}
	    </div>
          </div>    
        </div>
     );
};

export default Comment;
