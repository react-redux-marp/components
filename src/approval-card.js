import React from 'react';

const ApprovalCard = (props) => {

    let approvalText = props.approvalText || "Are you sure?";
    
    return (
      <div className="ui cards py-4">
        <div className="content w-100">{props.children}</div>
        <div className="extra content">
	  <label className="w-100">{approvalText}</label>  
          <div className="ui two buttons">
            <div className="ui basic green button">Approve</div>
            <div className="ui basic red button">Reject</div>
          </div>
        </div>
      </div>
    );
};

export default ApprovalCard;
