import React from 'react';
import ReactDOM from 'react-dom';
import Comment from './comment-detail';
import ApprovalCard from './approval-card';
import Faker from 'faker';

const App = () => {
    return (
      <div className="ui container comments pt-3">
        <ApprovalCard approvalText="sure?">
          <Comment author='Miguel' date='Today at 6:00pm' avatar={Faker.image.avatar()} content='First!'/>
        </ApprovalCard>
        <ApprovalCard>
          <Comment author='Sam' date='Today at 6:01pm' avatar={Faker.image.avatar()} content='Nice blog post!'/>    
        </ApprovalCard>
        <ApprovalCard>
          <Comment author='Jackie' date='Today at 6:02pm' avatar={Faker.image.avatar()} content='First?'/>    
        </ApprovalCard>
      </div>
    );
}

ReactDOM.render(
  <App/>,
  document.querySelector('#root')
);
